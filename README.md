# springmvc5.x_mybatisplus3.x_shiro_bootstrap4.x_ztree_ajax_jquery3.x_ssm_mysql
企业通用云平台系统（版本明确版）
学习对象：
         所有JAVA框架研发人员，想通过实战应对【java面试题】的活面试题，零基础的java学习路线
 
 
开发环境：Eclipse ，JDK 1.8 ，Tomcat7  
技术选型：  
后端技术  
核心框架：Spring Framework 4.X  
视图框架：Spring MVC 4.X  
任务调度：Spring + Quartz 2.2.3  
持久层框架：MyBatis 3.X + Mybatis-plus 2.1.8  
日志管理：SLF4J 1.7 + Log4j2 2.7  
工具类：Apache Commons、Jackson 2.2、fastjson 1.2.20  
前端技术  
JS框架：Jquery  
表格插件：Bootstrap Table  
表单验证插件：BootstrapValidator  
日期选择插件：Datepicker for Bootstrap  
弹层组件：Layer，LayUI  
数据图表：Echarts  
表单美化插件：ICheck  
树形视图插件：Ztree   
数据库      
      Mysql5+Druid  

IDE  
      Eclipse，MyEclipse，Intellij Idea    
JDK环境  
          jdk6以上  


Maven环境  
          Maven  

API  
         实现restful风格项目实例   
安全框架：Apache Shiro  
视图框架：Spring MVC  
持久层框架：MyBatis MyBatisPlus  
缓存技术：EhCache,Redis  
定时器：Quartz  
数据库连接池：Druid  
日志管理：SLF4J、Log4j  
模版技术：FreeMarker  
页面交互：BootStrap、Layer等  

本项目自己录制的视频教程地址： 

https：//study.163.com/course/courseMain.htm？share = 2＆ shareId = 0103275352＆courseId = 1006183001＆_trace_c_p_k2_ = c5f48d685b0c48aba2a0da1b73b46b68

![教程](https://images.gitee.com/uploads/images/2019/0123/200228_b60494f6_1822163.png "视频教程.png")